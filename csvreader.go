package main

/*
Read a csv file and dump its output somewhere else.
*/

import (
  "fmt"
  "encoding/csv"
  "flag"
  "os"
)

/* Cmdline flags. */
var src = flag.String("source", "./data.csv", "Source CSV file to read")
var dst = flag.String("dest", "./dump.csv", "Destination file")

func printHelp() {
  fmt.Println("Usage: ./csvreader -source file [-dest filename]")
  fmt.Println("\n\t-source file	  Path to CSV file to read from (default ./data.csv)")
  fmt.Println("\t-dest file	  Path to CSV file to where the content of source should be dumped (default ./dump.csv)")
}

func readCsvFile(name *string) ([][]string, error) {
  file, err := os.Open(*name)
  defer file.Close()
  if err != nil {
    return nil, err
  }

  r := csv.NewReader(file)
  content, err := r.ReadAll()
  if err != nil {
    return nil, err
  }

  return content, nil
}

func writeCsvFile(name *string, c [][]string) error {
  file, err := os.OpenFile(*name, os.O_WRONLY|os.O_CREATE, 0600)
  defer file.Close()
  if err != nil {
    return err
  }

  w := csv.NewWriter(file)
  err = w.WriteAll(c)
  if err != nil {
    return err
  }

  return nil
}

func main() {
  // Parse cmdline flags.
  flag.Parse()

  // Check number of arguments.
  if len(os.Args) <= 1 {
    printHelp()
    os.Exit(0)
  }

  // Read source csv file.
  content, err := readCsvFile(src)
  if err != nil {
    fmt.Println("Error failed to read:", err.Error())
    os.Exit(1)
  }

  // Dump content to new file.
  err = writeCsvFile(dst, content)
  if err != nil {
    fmt.Println("Error during writing:", err.Error())
    os.Exit(1)
  }

  fmt.Println("Dumped successful to", *dst)
  os.Exit(0)
}
